// Message
const addMessage = (userid, text) => ({
  type: 'ADD_MESSAGE',
  payload: {
    userid,
    text
  }
});

export {
  addMessage as default
};
