/* Initial states */
import { initialState as usersInitState } from 'reducers/users';
import { initialState as messagesInitState } from 'reducers/messages';

const initialState = {
  users: usersInitState,
  messages: messagesInitState
};

export default initialState;
