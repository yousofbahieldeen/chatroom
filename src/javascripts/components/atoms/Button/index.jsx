// Librairies
import React from 'react';
import PropTypes from 'prop-types';
// Styles
import './style.css';

const Button = ({ children, onClick, className }) => (
  <button className={className ? `${className} button` : 'button'} onClick={onClick}>{children}</button>
);

Button.propTypes = {
  className: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object
  ]),
  onClick: PropTypes.func,
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
    PropTypes.element
  ]),
};

Button.defaultProps = {
  className: null,
  children: '',
  onClick: () => {}
};


export default Button;
