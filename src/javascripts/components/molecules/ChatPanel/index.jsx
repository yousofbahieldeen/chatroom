// Librairies
import { connect } from 'react-redux';
// Components
import ChatPanel from './ChatPanel';

// Connect
const mapStateToProps = state => ({
    messages: state.messages
  }),
  ChatPanelConnector = connect(
    mapStateToProps
  )(ChatPanel);

export default ChatPanelConnector;
