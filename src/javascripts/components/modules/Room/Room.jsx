// Librairies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
// Components
import User from 'components/organisms/User';

// Styles
import './style.css';

// Render
const Room = ({ users }) => (
  <div className="room">
    {users.map(user => (
      <User key={user.id} profile={user} />
    ))}
  </div>
);

Room.propTypes = {
  users: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default Room;
